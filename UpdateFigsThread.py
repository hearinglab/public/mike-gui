import threading
import time
import matplotlib.pyplot as plt

class UpdateFigsThread(threading.Thread):
    def __init__(self, win):
        super(UpdateFigsThread, self).__init__()
        self._stopevent = threading.Event()
        self._sleepperiod = 0.02
        self.win = win
        print('Init Creating PlotSig Thread', flush=True)

    def stop(self):
        print('Setting Stop Flag', flush=True)
        try:
            self._stopevent.set()
        except Exception as e:
            print(e)

    def run(self):
        print('Running UpdateFigsThread')        
        try:
            self._stopevent.clear()
            while not self._stopevent.is_set():
                #print('UpdateFigsThread update', flush=True)
                self.win.plotData()
                if self._stopevent.wait(timeout=self._sleepperiod):
                    break
            print('UpdateFigsThread loop terminated', flush=True)

                
        except Exception as e:
            print(e)
