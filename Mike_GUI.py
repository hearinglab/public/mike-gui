import sys
import pyaudio
import numpy as np
import time
import threading
import DOA

from PyQt5.QtWidgets import QApplication, QMainWindow

import StreamAudio as SA
import matplotlib.pyplot as plt

import UpdateFigsThread as UFT

from Mike_Main_ui import Ui_MainWindow

class Window(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)

        "Collect audio device info"
        self.a = SA.StreamAudio()
        self.ADev = []
        for k in range(self.a.p.get_device_count()):
            self.ADev.append(self.a.p.get_device_info_by_index(k))

        "Populate comboDevice"
        self.comboDevice.clear()
        for k in range(self.a.p.get_device_count()):
            self.comboDevice.addItem(self.ADev[k]['name'])
        self.GetDevInfo()

        "Connect UI events to functions"
        self.connectSignalsSlots()

        self.buttonStop.setEnabled(False)
        self.buttonStart.setEnabled(True)
        
        "Set up plot objects"
        SigA = self.a.MicSig[0,:]
        SigB = self.a.MicSig[1,:]
        t = np.arange(len(SigA))/self.a.Rate
        
        self.ChASpark, = self.widgetFigChA.canvas.ax.plot(SigA)
        self.widgetFigChA.canvas.ax.set_ylim([-32727, 32727])
        self.widgetFigChA.canvas.ax.axis('off')
        self.widgetFigChA.canvas.ax.set_position((0,0,1,1))
        self.widgetFigChB.canvas.fig.set_constrained_layout(True)
        self.widgetFigChA.canvas.draw()
        
        self.ChBSpark, = self.widgetFigChB.canvas.ax.plot(SigB)
        self.widgetFigChB.canvas.ax.set_ylim([-32727, 32727])
        self.widgetFigChB.canvas.ax.axis('off')
        self.widgetFigChB.canvas.ax.set_position((0,0,1,1))
        self.widgetFigChB.canvas.fig.set_constrained_layout(True)
        self.widgetFigChB.canvas.draw()

        self.D = DOA.DOA_Param
        self.D.fs = self.a.Rate

        self.lineEditWinDur.setText(str(self.D.winDur))
        self.lineEditOverlapProp.setText(str(self.D.OverlapProp))
        

    def connectSignalsSlots(self):
        self.comboDevice.currentIndexChanged.connect(self.GetDevInfo)
        self.buttonStart.clicked.connect(self.Start)
        self.buttonStop.clicked.connect(self.Stop)
        self.comboChA.currentIndexChanged.connect(self.UpdateCh)
        self.comboChB.currentIndexChanged.connect(self.UpdateCh)
        
        
    def GetDevInfo(self):
            AIdx = self.comboDevice.currentIndex()
            NInChannels = self.ADev[AIdx]['maxInputChannels']
            self.a.Device = AIdx
            if NInChannels > 0:
                MessageString = (str(NInChannels) + '   ')
            else:
                MessageString = ('None')
            self.labelNumInputs.setText(MessageString)
            self.comboChA.clear()
            self.comboChB.clear()
            for k in range(NInChannels):
                self.comboChA.addItem(str(k))
                self.comboChB.addItem(str(k))


    def UpdateCh(self):
        try:
            self.a.ChA = self.comboChA.currentIndex()
            self.a.ChB = self.comboChB.currentIndex()
        except Exception as e:
            print(e)
            
        
    def Start(self):

        'Disable Start button and enable Stop'
        self.buttonStop.setEnabled(True)
        self.buttonStart.setEnabled(False)

        "Initialize main figure"
        self.D.winDur = int(self.lineEditWinDur.text())
        self.D.OverlapProp = float(self.lineEditOverlapProp.text())
        
        # remember to update any changes to self.D (DOA_Param)
        try:
            
            SigA = self.a.MicSig[0,:]
            SigB = self.a.MicSig[1,:]
            xc, lags, T = DOA.XCorrDOA(SigA, SigB, self.D)
            '''
            print(xc.shape, flush=True)
            print(len(T), flush=True)
            print(lags[0], flush=True)
            print(lags[-1], flush=True)
            '''
            self.widgetFig1.canvas.ax.cla()
            DOA_indices = np.argmax(xc, axis=0)
            self.DOA_plot, = self.widgetFig1.canvas.ax.plot(lags[DOA_indices], 'bo')
            self.widgetFig1.canvas.ax.set_ylim(lags[0], lags[-1])
            self.widgetFig1.canvas.show()
        except Exception as e:
            print(e)
        

        "Start thread to continuously update figures in the background"
        try:
            self.UpdateFigsThread = UFT.UpdateFigsThread(self)
            self.UpdateFigsThread.start()
        except Exception as e:
            print(e)
            
        "Update audio parameters" 
        self.a.MicSiglength = 2
        self.a.Rate = 44100
        self.a.Format = pyaudio.paInt16
        self.a.Chunk = 4096
        self.a.Device = self.comboDevice.currentIndex()

        "Start audio streaming"
        StatusString = 'Starting Audio Device:' + str(self.a.Rate) + ' Hz'
        self.statusBar().showMessage(StatusString, 2000)
        self.a.start_audio_stream()
        

    def plotData(self):

        try:
            tempdata = self.a.MicSig
            SigA = tempdata[0,:]
            SigB = tempdata[1,:]
            
            xc, lags, T = DOA.XCorrDOA(SigA, SigB, self.D)
            self.DOA_plot.set_ydata(lags[np.argmax(xc, axis=0)])
            self.widgetFig1.canvas.draw()

            self.ChASpark.set_ydata(SigA)
            self.widgetFigChA.canvas.draw()

            self.ChBSpark.set_ydata(SigB)
            self.widgetFigChB.canvas.draw()          

        except Exception as e:
            print(e)

            
    def Stop(self):

        # Debug code to check what threads are active
        #print(threading.enumerate(), flush=True)

        'Disable Stop button and enable Start'
        self.buttonStop.setEnabled(False)
        self.buttonStart.setEnabled(True)

        'Stop audio and kill display threads?'
        try:   
            self.a.stop_audio_stream()
            print('Stopping stream')
        except:
            print('No stream to stop')

        try:
            self.UpdateFigsThread.stop()
            self.plotData()
        except Exception as e:
            print(e)
        

if __name__ == "__main__":
    app = QApplication(sys.argv)
    win = Window()
    win.show()
    sys.exit(app.exec())
