import pyaudio
import numpy as np

class StreamAudio(object):
    """
    Continuously update a stereo buffer from microphone signals.
    """

    def __init__(self):       
        "Set some defaults. These can be changed before calling stream_start"        
        self.MicSigLength = 2 # Duraton of past audio stream that is maintained
        self.Rate = 44100 # sampling rate in Hz
        self.Format = pyaudio.paInt16
        self.Chunk = 4096 # length of input buffer in samples collect before updating
        self.MicSig = np.zeros([2,int(round(self.Rate*self.MicSigLength))])
           
        "ChA and ChB are labels for the two channels in buffer"
        self.ChA = 0
        self.ChB = 0

        self.p=pyaudio.PyAudio()
        self.Device = self.p.get_default_input_device_info()['index']
        print("Initiating audio, Default device: " + self.p.get_device_info_by_index(self.Device)['name'])
        
    def callback(self, in_data, frame_count, time_info, status):
        "Write recorded data to MicSig buffer"
        data = np.fromstring(in_data,dtype=np.int16)
        '''
        For multichannel devices we get samples from all inputs,
        with samples interleaved. Reshape the vector to a matrix
        '''
        data = np.reshape(data, (self.MaxInputCh,self.Chunk),'F')

        '''
        Write to MicSig. Note other threads may read this buffer during the
        update. Could try to add lock to avoid this.
        '''
        tempdata = self.MicSig
        tempdata[:,:-self.Chunk]=tempdata[:,self.Chunk:]
        tempdata[0,-self.Chunk:]=data[self.ChA,:]
        tempdata[1,-self.Chunk:]=data[self.ChB,:]
        self.MicSig = tempdata

        '''
        Check status flags
        '''
        if status == pyaudio.paInputUnderflow:
            print("PyAudio input underflow")
        elif status == pyaudio.paInputOverflow:
            print("PyAudio input overlfow")
        
        return in_data, pyaudio.paContinue

    
    def start_audio_stream(self):
        "Update variables based on current UI values"
        self.MicSig = np.zeros([2,int(round(self.Rate*self.MicSigLength))])
        self.MaxInputCh = self.p.get_device_info_by_index(self.Device)['maxInputChannels']

        print('Starting stream with Device: ' + str(self.Device), flush=True)
        print('Number of input channels: ' + str(self.MaxInputCh), flush=True)
        print('Using Channels A: ' + str(self.ChA) + '  B: ' + str(self.ChB), flush=True) 

        try:
            self.stream=self.p.open(input_device_index=self.Device,
                                format=self.Format,
                                channels=self.MaxInputCh,
                                rate=self.Rate,input=True,
                                frames_per_buffer=self.Chunk,
                                stream_callback=self.callback)
        except Exception as e:
            print(e)
        
    def stop_audio_stream(self):
        print('Stopping stream', flush=True)
        self.stream.stop_stream()
        self.stream.close()
        print(" -- stream closed")

    def close(self):
        self.stream_stop()
        #self.p.terminate()
