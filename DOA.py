import numpy as np
from scipy import signal

class DOA_Param:
    winDur = 100 # window length in ms
    OverlapProp = 0.8
    fs = 44100 # assumed sampling rate
    c = 340 # Speed of sound
    r = 0.1 # Radius of head

def XCorrDOA(SigA, SigB, DOA_Param):
    win_len = round(DOA_Param.fs*DOA_Param.winDur/1000)
    overlap = round(win_len*DOA_Param.OverlapProp)
    maxlag = 1.5 * np.ceil(DOA_Param.fs*2*DOA_Param.r/DOA_Param.c)
    SigABuffer = buffer(SigA, win_len, overlap)
    SigBBuffer = buffer(SigB, win_len, overlap)
    #print(SigABuffer.shape)
    xc = np.zeros(SigABuffer.shape)
    for k in range(xc.shape[1]):
        xc[:,k] = signal.correlate(SigABuffer[:,k], SigBBuffer[:,k], 'same', 'fft')
    lags = signal.correlation_lags(SigABuffer[:,k].size, SigBBuffer[:,k].size, 'same')
    T = np.arange(win_len/(2*DOA_Param.fs), len(SigA)/DOA_Param.fs,
                  (win_len-overlap)/DOA_Param.fs)
    
    startlag = np.min(np.where(lags > -maxlag))
    stoplag = np.max(np.where(lags < maxlag))
    
    return xc[startlag:stoplag,:], lags[startlag:stoplag], T

def buffer(X = np.array([]), n = 1, p = 0):
    #buffers data vector X into length n column vectors with overlap p
    #excess data at the end of X is discarded
    n = int(n) #length of each data vector
    p = int(p) #overlap of data vectors, 0 <= p < n-1
    L = len(X) #length of data to be buffered
    m = int(np.floor((L-n)/(n-p)) + 1) #number of sample vectors (no padding)
    data = np.zeros([n,m]) #initialize data matrix
    for startIndex,column in zip(range(0,L-n,n-p),range(0,m)):
        data[:,column] = X[startIndex:startIndex + n] #fill in by column
    return data

